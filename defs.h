#ifndef das
#define das

#include <cstdlib>

const size_t MAX_TASK = 32;
const size_t MAX_RES = 16;
const size_t MAX_EVENT = 16;

#endif
